<?php
/*
* Theme customised functions
* By Archita
*/

add_action( 'storefront_header', 'storefront_header_cart', 40 );

add_action( 'init', 'jk_remove_storefront_header_search' );
function jk_remove_storefront_header_search() {
	remove_action( 'storefront_header', 'storefront_product_search', 40 );
	remove_action( 'storefront_header', 'storefront_header_cart', 60 );
	remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );//remove sidebar
	//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	/*if( is_page( 12 ) ){*/
		/*remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 42 );
		remove_action( 'storefront_header', 'storefront_primary_navigation', 50 );
		remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 68 );*/
	/*}*/

}

function homecat_html() {
	global $wpdb;
	$response = "";
	$taxonomyName = "product_cat";
	$prod_categories = get_terms($taxonomyName, array(
	     'hide_empty' => 1,
      	'exclude' => array( 15,29,33,32,30),
        'meta_key' => 'homepage_order',
        'orderby' => 'homepage_order'
	));
   $i =1;
	foreach( $prod_categories as $prod_cat ) :
    if ( $prod_cat->parent != 0 )
        continue;
	    $cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
	    if($i == 1){
	    $cat_thumb_url = wp_get_attachment_image_src( $cat_thumb_id, array('598','285') )[0]; // Change to desired 'thumbnail-size'

	    }else{
	    $cat_thumb_url = wp_get_attachment_image_src( $cat_thumb_id, array('400','400') )[0]; // Change to desired 'thumbnail-size'

	    }
	    $term_link = get_term_link( $prod_cat, 'product_cat' );
	    $i++;

		$response .= '<a class="product" href="'.$term_link.'">';
		$response .= '<img  src="'.$cat_thumb_url.'" alt="" />';
	 	$response .= '<h4>'.$prod_cat->name .'</h4>';
		$response .= '</a>';
	endforeach;
	wp_reset_query();


	//echo $_POST['post_id'];
	echo $response;
	wp_die();
}

add_action( 'wp_ajax_showPro_pop', 'homecat_html' );
add_action( 'wp_ajax_nopriv_showPro_pop', 'homecat_html' );

// define the woocommerce_after_shop_loop_item callback
function action_woocommerce_after_shop_loop_item() {
    $category = get_queried_object();
    if(!is_shop()){
    	$currentId = $category->term_id;
    }else{
    	$currentId = '';
    }
    $taxonomyName = "product_cat";
    $prod_categories = get_terms($taxonomyName, array(
      'exclude' => array( 15,29,33,32,30),
        'hide_empty' => 1,
	    //'exclude' => array( 15,26,23),
        'meta_key' => 'shop_page_order',
        'orderby' => 'shop_page_order'
    ));

    foreach( $prod_categories as $prod_cat ) {
    if ( $prod_cat->parent != 0 )
        continue;
    if($prod_cat->term_id !== $currentId){
    echo '<h2 class="woocommerce-products-header__title page-title">'.$prod_cat->name .'</h2>';
     echo do_shortcode('[products limit="4" columns="4" category='.$prod_cat->term_id.']');
    }
}
         }
// add the action
add_action( 'woocommerce_after_main_content', 'action_woocommerce_after_shop_loop_item' );


// Add the opening div to the title price
function add_title_wrapper_start() {
    echo '<div class="archive-title-wrap">';
}
add_action( 'woocommerce_shop_loop_item_title', 'add_title_wrapper_start', 5, 2 );

// Close the div that we just added
function add_title_wrapper_close() {
    echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item_title', 'add_title_wrapper_close', 12, 2 );



add_filter( 'woocommerce_is_purchasable', '__return_false');
/**
 * Hide loop read more buttons for out of stock items 
 */
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
	function woocommerce_template_loop_add_to_cart() {
		global $product;
		if ( ! $product->is_in_stock() || ! $product->is_purchasable() ) return;
		wc_get_template('loop/add-to-cart.php');
	}
}
add_action( 'template_redirect', 'product_redirection_to_home', 100 );
function product_redirection_to_home() {
    //if ( ! is_product() ) return; // Only for single product pages.
	if(is_product() || is_cart() || is_checkout()){
		wp_redirect( home_url() );
	}
     // redirect home.
    //exit();
}

