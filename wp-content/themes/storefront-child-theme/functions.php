<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}
function custom_style(){

	wp_enqueue_style(
      'wpse_89494_style_1', get_stylesheet_directory_uri() . '/assets/css/style.css'
    );
    wp_enqueue_script( 'ajax-script', get_stylesheet_directory_uri() .  '/assets/js/homecat.js', array('jquery') );
	wp_localize_script( 'ajax-script', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
	//wp_localize_script( 'ajax-script', 'ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) );

}

add_action('wp_footer','footer_scripts');
wp_enqueue_script('jquery');

function footer_scripts(){
	?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/owl.carousel.min.css">
    <script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/owl.carousel.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/owl.carousel2.thumbs.js"></script>
<?php }

add_action( 'wp_enqueue_scripts', 'custom_style' );

/*function google_ana(){
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179307827-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-179307827-1');
</script>

<?php }
add_action('wp_head','google_ana');*/
require 'assets/functions/woo-archive.php';



/******woo tabs in popup*****/
add_action('wp_footer','wootabs');
function wootabs(){
    ?>
    <script>
var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":"1"};
</script>
<script type="text/javascript" src='<?php echo site_url() ?>/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js'>

</script>
    <?php
}

add_filter( 'woocommerce_product_tabs', 'woo_custom_product_tabs' );
function woo_custom_product_tabs( $tabs ) {

    // 1) Removing tabs

    unset( $tabs['description'] );              // Remove the description tab
    // unset( $tabs['reviews'] );               // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab


    // 2 Adding new tabs and set the right order
// Adds the qty pricing  tab
    $tabs['qty_pricing_tab'] = array(
        'title'     => __( 'What’s In It', 'woocommerce' ),
        'priority'  => 100,
        'callback'  => 'woo_attrib_desc_tab_content'
    );
    //Attribute Description tab
    $tabs['attrib_desc_tab'] = array(
        'title'     => __( 'Allergens', 'woocommerce' ),
        'priority'  => 110,
        'callback'  => 'woo_qty_pricing_tab_content'
    );



    // Adds the other products tab
    /*$tabs['other_products_tab'] = array(
        'title'     => __( 'Nutrition', 'woocommerce' ),
        'priority'  => 120,
        'callback'  => 'woo_other_products_tab_content'
    );*/

    return $tabs;

}

// New Tab contents

function woo_attrib_desc_tab_content() {
    global $product;
	$id = $product->get_id();
    //echo $id;
    the_field('ingredients', $id);
}
function woo_qty_pricing_tab_content() {
    global $product;
	$id = $product->get_id();
    //echo $id;
    the_field('dietary_info', $id);
}
/*function woo_other_products_tab_content() {
    global $product;
	$id = $product->get_id();
    //echo $id;
    the_field('nutrition', $id);
}*/

function filter_woocommerce_add_to_cart_fragments( $fragments ) {
    $cart = WC()->cart->get_cart();
    global $woocommerce;
   // return  var_dump(WC()->cart->cart_contents);
   $cartval = array();

    foreach ( WC()->cart->get_cart() as $cart_item ) {
         $kkey = array_search($cart_item['product_id'], array_column($cartval, 'pid'));
         if(!$cart_item['wooco_qty']){
            if($kkey !== false){
              $cartval[$kkey]['qty']= $cartval[$kkey]['qty'] + 1;
            }else{

             $cartval[] = array('pid' => $cart_item['product_id'],'qty' => $cart_item['quantity']);
            }
        }

    }

    $fragments['in_cart_qty'] = $cartval;
    return $fragments;
};

// add the filter
add_filter( 'woocommerce_add_to_cart_fragments', 'filter_woocommerce_add_to_cart_fragments', 10, 1 );

// Removing the google font
function remove_font(){
  wp_dequeue_style('storefront-fonts');
}
add_action( 'wp_enqueue_scripts', 'remove_font', 999);


// Adding class for cart button
if ( ! function_exists( 'storefront_header_cart' ) ) {
    /**
     * Display Header Cart
     *
     * @since  1.0.0
     * @uses  storefront_is_woocommerce_activated() check if WooCommerce is activated
     * @return void
     */
    function storefront_header_cart() {
        if ( storefront_is_woocommerce_activated() ) {
            if ( is_cart() ) {
                $class = 'current-menu-item';
            } else {
                $class = '';
            }
            ?>
        <ul id="site-header-cart" class="site-header-cart menu">
            <li class="<?php echo esc_attr( $class ); ?> mobile-fixed">
                <?php storefront_cart_link(); ?>
            </li>
            <li>
                <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
            </li>
        </ul>
            <?php
        }
    }
}
function storefront_credit() {
    ?>
    <div class="site-info">
     <a href="<?php echo site_url(); ?>" target="_blank" title="Really Good Juice" rel="author">© Really Good Juice 2020</a>
    </div><!-- .site-info -->
    <?php
}
