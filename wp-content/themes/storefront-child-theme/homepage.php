<?php
/*
* Template name: Homepage
*
*/
get_header();

?>

<div class="row home-product">

</div>
<div class="as-seen">
  <h2>As seen in:</h2>
  <div class="logo-slider owl-carousel">
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/Big.png">
    </div>
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/peta.png">
    </div>
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/vegnews.png">
    </div>
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/TVB.png">
    </div>
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/vegan-food.png">
    </div>
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/vegan-living.png">
    </div>
    <div class="item">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logos/livekindly.png">
    </div>
  </div>
</div>
<script>
jQuery(document).ready(function($) {
  $('.logo-slider').owlCarousel({
    loop: true,
    nav:false,
    dots:false,
    margin:20,
    responsive: {
      320: {
        items: 2.5
      },
      600: {
        items: 3.5
      },
      991: {
        items: 6.5
      }
    }
  });
 });
</script>

<?php get_footer(); ?>
