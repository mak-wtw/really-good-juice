<?php
   /*
   Plugin Name: Checkout custom Fields
   Version: 1.1
   Description: Adding eat-in,collection and delivery fields and functionality
   Author: Archita

   */

register_activation_hook( __FILE__, 'checkout_custom_activate' );
function checkout_custom_activate() {  
    // Prevent plugin activation if the minimum PHP version requirement is not met.
    if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
        deactivate_plugins( basename( __FILE__ ) );
        $msg = '<p><strong> Checkout page custom fields for WooCommerce</strong> requires PHP version 5.4 or greater. Your server runs ' . PHP_VERSION . '.</p>';
        wp_die( $msg, 'Plugin Activation Error',  array( 'response' => 200, 'back_link' => TRUE ) );
    }
    // Store time of first plugin activation (add_option does nothing if the option already exists).
    add_option( 'checkout_custom_first_activate', time());
}


function create_checkout_field()
{
   $labels = array(
    'name'               => _x( 'checkout fields', 'post type general name', 'stacy' ),
    'singular_name'      => _x( 'checkout field', 'post type singular name', 'stacy' ),
    'menu_name'          => _x( 'checkout fields', 'admin menu', 'stacy' ),
    'name_admin_bar'     => _x( 'checkout fields', 'add new on admin bar', 'stacy' ),
    'add_new'            => _x( 'Add New', 'checkout fields', 'stacy' ),
    'add_new_item'       => __( 'Add New field', 'stacy' ),
    'new_item'           => __( 'New field', 'stacy' ),
    'edit_item'          => __( 'Edit field', 'stacy' ),
    'view_item'          => __( 'View field', 'stacy' ),
    'all_items'          => __( 'All field', 'stacy' ),
    'search_items'       => __( 'Search field', 'stacy' ),
    'not_found'          => __( 'No field found.', 'stacy' ),
    'not_found_in_trash' => __( 'No field found in Trash.', 'stacy' )
);

    $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'Add New field on stacy' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'check-field' ),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 100,
            'menu_icon'          =>'dashicons-cart',
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail','comments','capabilities' ),
            'taxonomies'         => array('field_category','field_tag')
);
    register_post_type( 'Checkout Fields', $args );
 }
 add_action( 'init', 'create_checkout_field' );


/**
 * Add the field to the checkout
 
add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {

    echo '<div id="my_custom_checkout_field">';

    woocommerce_form_field( the_field('shipping_order',82),
    ), $checkout->get_value( 'shipping_order' ) );

    echo '</div>';

}
// Process the checkout
 
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['my_field_name'] )
        wc_add_notice( __( 'Please enter something into this new shiny field.' ), 'error' );
}**/