# Copyright (C) 2020 WPClever.net
# This file is distributed under the same license as the WPC Composite Products for WooCommerce plugin.
msgid ""
msgstr ""
"Project-Id-Version: WPC Composite Products for WooCommerce 3.0.7\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wpc-composite-products\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-06-29T22:51:28+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.2.0\n"
"X-Domain: wpc-composite-products\n"

#. Plugin Name of the plugin
msgid "WPC Composite Products for WooCommerce"
msgstr ""

#. Plugin URI of the plugin
msgid "https://wpclever.net/"
msgstr ""

#. Description of the plugin
msgid "WPC Composite Products provide a powerful kit-building solution for WooCommerce store."
msgstr ""

#. Author of the plugin
msgid "WPClever.net"
msgstr ""

#. Author URI of the plugin
msgid "https://wpclever.net"
msgstr ""

#: wpc-composite-products.php:63
#: wpc-composite-products.php:859
msgid "Select options"
msgstr ""

#: wpc-composite-products.php:69
#: wpc-composite-products.php:865
msgid "Read more"
msgstr ""

#: wpc-composite-products.php:80
#: wpc-composite-products.php:876
msgid "Add to cart"
msgstr ""

#: wpc-composite-products.php:374
msgid "remove"
msgstr ""

#: wpc-composite-products.php:379
#: wpc-composite-products.php:418
msgid "Name"
msgstr ""

#: wpc-composite-products.php:388
msgid "Description"
msgstr ""

#: wpc-composite-products.php:397
msgid "Source"
msgstr ""

#: wpc-composite-products.php:401
msgid "Select source"
msgstr ""

#: wpc-composite-products.php:403
msgid "Products"
msgstr ""

#: wpc-composite-products.php:406
msgid "Categories"
msgstr ""

#: wpc-composite-products.php:409
msgid "Tags"
msgstr ""

#: wpc-composite-products.php:413
msgid "Order by"
msgstr ""

#: wpc-composite-products.php:415
#: wpc-composite-products.php:426
msgid "Default"
msgstr ""

#: wpc-composite-products.php:416
msgid "None"
msgstr ""

#: wpc-composite-products.php:417
msgid "ID"
msgstr ""

#: wpc-composite-products.php:419
msgid "Type"
msgstr ""

#: wpc-composite-products.php:420
msgid "Rand"
msgstr ""

#: wpc-composite-products.php:421
msgid "Date"
msgstr ""

#: wpc-composite-products.php:422
msgid "Modified"
msgstr ""

#: wpc-composite-products.php:424
msgid "Order"
msgstr ""

#: wpc-composite-products.php:427
msgid "DESC"
msgstr ""

#: wpc-composite-products.php:428
msgid "ASC"
msgstr ""

#: wpc-composite-products.php:439
#: wpc-composite-products.php:499
msgid "Search for a product&hellip;"
msgstr ""

#: wpc-composite-products.php:462
msgid "Search for a category&hellip;"
msgstr ""

#: wpc-composite-products.php:483
msgid "Add some tags, split by a comma..."
msgstr ""

#: wpc-composite-products.php:490
msgid "Default option"
msgstr ""

#: wpc-composite-products.php:516
msgid "Required"
msgstr ""

#: wpc-composite-products.php:521
#: wpc-composite-products.php:561
#: wpc-composite-products.php:711
#: wpc-composite-products.php:732
#: wpc-composite-products.php:742
#: wpc-composite-products.php:752
#: wpc-composite-products.php:771
#: wpc-composite-products.php:804
#: wpc-composite-products.php:935
#: wpc-composite-products.php:969
msgid "Yes"
msgstr ""

#: wpc-composite-products.php:524
#: wpc-composite-products.php:558
#: wpc-composite-products.php:712
#: wpc-composite-products.php:733
#: wpc-composite-products.php:743
#: wpc-composite-products.php:753
#: wpc-composite-products.php:772
#: wpc-composite-products.php:812
#: wpc-composite-products.php:837
#: wpc-composite-products.php:890
#: wpc-composite-products.php:939
#: wpc-composite-products.php:958
#: wpc-composite-products.php:973
msgid "No"
msgstr ""

#: wpc-composite-products.php:531
msgid "New price"
msgstr ""

#: wpc-composite-products.php:538
msgid "Set a new price using a number (eg. \"49\" for $49) or a percentage (eg. \"90%\" of the original price)."
msgstr ""

#: wpc-composite-products.php:543
#: wpc-composite-products.php:1715
msgid "Quantity"
msgstr ""

#: wpc-composite-products.php:553
msgid "Custom quantity"
msgstr ""

#: wpc-composite-products.php:568
msgid "Min"
msgstr ""

#: wpc-composite-products.php:578
msgid "Max"
msgstr ""

#: wpc-composite-products.php:593
#: wpc-composite-products.php:604
msgid "WPC Composite Products"
msgstr ""

#: wpc-composite-products.php:593
msgid "Composite Products"
msgstr ""

#: wpc-composite-products.php:607
msgid "Thank you for using our plugin! If you are satisfied, please reward it a full five-star %s rating."
msgstr ""

#: wpc-composite-products.php:610
msgid "Reviews"
msgstr ""

#: wpc-composite-products.php:612
msgid "Changelog"
msgstr ""

#: wpc-composite-products.php:614
msgid "Discussion"
msgstr ""

#: wpc-composite-products.php:621
msgid "How to use?"
msgstr ""

#: wpc-composite-products.php:625
#: wpc-composite-products.php:1070
msgid "Settings"
msgstr ""

#: wpc-composite-products.php:630
#: wpc-composite-products.php:1071
msgid "Premium Version"
msgstr ""

#: wpc-composite-products.php:638
msgid "When creating the product, please choose product data is \"Composite product\" then you can see the search field to start search and add component products."
msgstr ""

#: wpc-composite-products.php:674
msgid "General"
msgstr ""

#: wpc-composite-products.php:678
msgid "Price format"
msgstr ""

#: wpc-composite-products.php:681
msgid "From regular price"
msgstr ""

#: wpc-composite-products.php:682
msgid "From sale price"
msgstr ""

#: wpc-composite-products.php:683
msgid "Regular and sale price"
msgstr ""

#: wpc-composite-products.php:686
msgid "Choose a price format for composites on the archive page."
msgstr ""

#: wpc-composite-products.php:691
msgid "Selector interface"
msgstr ""

#: wpc-composite-products.php:694
msgid "ddSlick"
msgstr ""

#: wpc-composite-products.php:695
msgid "Select2"
msgstr ""

#: wpc-composite-products.php:696
msgid "HTML select tag"
msgstr ""

#: wpc-composite-products.php:708
msgid "Exclude unpurchasable"
msgstr ""

#: wpc-composite-products.php:714
msgid "Exclude unpurchasable products from the list."
msgstr ""

#: wpc-composite-products.php:718
msgid "Show alert"
msgstr ""

#: wpc-composite-products.php:721
msgid "On composite loaded"
msgstr ""

#: wpc-composite-products.php:722
msgid "On composite changing"
msgstr ""

#: wpc-composite-products.php:723
msgid "No, always hide the alert"
msgstr ""

#: wpc-composite-products.php:725
msgid "Show the inline alert under the components."
msgstr ""

#: wpc-composite-products.php:729
msgid "Show quantity"
msgstr ""

#: wpc-composite-products.php:735
msgid "Show the quantity before product name."
msgstr ""

#: wpc-composite-products.php:739
msgid "Show image"
msgstr ""

#: wpc-composite-products.php:745
#: wpc-composite-products.php:755
msgid "Only for HTML select tag."
msgstr ""

#: wpc-composite-products.php:749
msgid "Show price"
msgstr ""

#: wpc-composite-products.php:759
msgid "Option none"
msgstr ""

#: wpc-composite-products.php:763
#: wpc-composite-products.php:2128
msgid "No, thanks. I don't need this"
msgstr ""

#: wpc-composite-products.php:764
msgid "Text to display for showing a \"Don't choose any product\" option."
msgstr ""

#: wpc-composite-products.php:768
msgid "Show \"Option none\" for required component"
msgstr ""

#: wpc-composite-products.php:777
msgid "Total text"
msgstr ""

#: wpc-composite-products.php:781
#: wpc-composite-products.php:1016
msgid "Total price:"
msgstr ""

#: wpc-composite-products.php:783
#: wpc-composite-products.php:794
#: wpc-composite-products.php:851
msgid "Leave blank to use the default text and its equivalent translation in multiple languages."
msgstr ""

#: wpc-composite-products.php:788
msgid "Saved text"
msgstr ""

#: wpc-composite-products.php:792
#: wpc-composite-products.php:1020
msgid "(saved [d])"
msgstr ""

#: wpc-composite-products.php:799
msgid "Change price"
msgstr ""

#: wpc-composite-products.php:808
msgid "Yes, custom selector"
msgstr ""

#: wpc-composite-products.php:819
msgid "Change the main product’s price based on the changes in prices of selected variations in a grouped products. This uses Javascript to change the main product’s price to it depends heavily on theme’s HTML. If the price doesn't change when this option is enabled, please contact us and we can help you adjust the JS file. "
msgstr ""

#: wpc-composite-products.php:824
msgid "Link to individual product"
msgstr ""

#: wpc-composite-products.php:829
msgid "Yes, open product page"
msgstr ""

#: wpc-composite-products.php:833
msgid "Yes, open quick view popup"
msgstr ""

#: wpc-composite-products.php:840
msgid "Add a link to the target individual product below this selection."
msgstr ""

#: wpc-composite-products.php:848
msgid "\"Add to Cart\" button labels"
msgstr ""

#: wpc-composite-products.php:855
msgid "Archive/shop page"
msgstr ""

#: wpc-composite-products.php:861
msgid "For purchasable composites."
msgstr ""

#: wpc-composite-products.php:867
msgid "For unpurchasable composites."
msgstr ""

#: wpc-composite-products.php:872
msgid "Single product page"
msgstr ""

#: wpc-composite-products.php:881
msgid "Cart & Checkout"
msgstr ""

#: wpc-composite-products.php:885
msgid "Coupon restrictions"
msgstr ""

#: wpc-composite-products.php:894
msgid "Exclude composite"
msgstr ""

#: wpc-composite-products.php:898
msgid "Exclude component products"
msgstr ""

#: wpc-composite-products.php:902
msgid "Exclude both composite and component products"
msgstr ""

#: wpc-composite-products.php:906
msgid "Choose products you want to exclude from coupons."
msgstr ""

#: wpc-composite-products.php:911
msgid "Cart content count"
msgstr ""

#: wpc-composite-products.php:916
msgid "Composite only"
msgstr ""

#: wpc-composite-products.php:920
msgid "Component products only"
msgstr ""

#: wpc-composite-products.php:924
msgid "Both composite and component products"
msgstr ""

#: wpc-composite-products.php:930
msgid "Hide composite name before component products"
msgstr ""

#: wpc-composite-products.php:945
msgid "Hide component products on cart & checkout page"
msgstr ""

#: wpc-composite-products.php:950
msgid "Yes, just show the composite"
msgstr ""

#: wpc-composite-products.php:954
msgid "Yes, but show component product names under the composite"
msgstr ""

#: wpc-composite-products.php:964
msgid "Hide component products on mini-cart"
msgstr ""

#: wpc-composite-products.php:977
msgid "Hide component products, just show the main composite on mini-cart."
msgstr ""

#: wpc-composite-products.php:984
msgid "Update Options"
msgstr ""

#: wpc-composite-products.php:1043
msgid "Please choose at least [min] of the whole products before adding to the cart."
msgstr ""

#: wpc-composite-products.php:1044
msgid "Please choose maximum [max] of the whole products before adding to the cart."
msgstr ""

#: wpc-composite-products.php:1045
msgid "Please select different product for each component."
msgstr ""

#: wpc-composite-products.php:1046
msgid "Please select a purchasable product in the component [name] before adding to the cart."
msgstr ""

#: wpc-composite-products.php:1087
msgid "Premium support"
msgstr ""

#: wpc-composite-products.php:1277
msgid "One of the component products is unavailable."
msgstr ""

#: wpc-composite-products.php:1278
#: wpc-composite-products.php:1285
#: wpc-composite-products.php:1292
#: wpc-composite-products.php:1299
#: wpc-composite-products.php:1306
#: wpc-composite-products.php:1316
#: wpc-composite-products.php:1324
msgid "You cannot add this composite products to the cart."
msgstr ""

#: wpc-composite-products.php:1284
#: wpc-composite-products.php:1291
msgid "\"%s\" is un-purchasable."
msgstr ""

#: wpc-composite-products.php:1298
#: wpc-composite-products.php:1315
msgid "\"%s\" has not enough stock."
msgstr ""

#: wpc-composite-products.php:1305
msgid "You cannot add another \"%s\" to your cart."
msgstr ""

#: wpc-composite-products.php:1323
msgid "\"%s\" is protected and cannot be purchased."
msgstr ""

#: wpc-composite-products.php:1503
#: wpc-composite-products.php:1524
#: wpc-composite-products.php:1628
#: wpc-composite-products.php:2469
#: wpc-composite-products.php:2481
msgid "Components"
msgstr ""

#: wpc-composite-products.php:1562
msgid "(in %s)"
msgstr ""

#: wpc-composite-products.php:1592
msgid "Composite (%s)"
msgstr ""

#: wpc-composite-products.php:1621
msgid "Composite product"
msgstr ""

#: wpc-composite-products.php:1660
msgid "+ Add component"
msgstr ""

#: wpc-composite-products.php:1670
msgid "Always put a price in the General tab to display the Add to Cart button. This is also the base price."
msgstr ""

#: wpc-composite-products.php:1674
msgid "Pricing"
msgstr ""

#: wpc-composite-products.php:1677
msgid "Only base price"
msgstr ""

#: wpc-composite-products.php:1678
msgid "Include base price"
msgstr ""

#: wpc-composite-products.php:1679
msgid "Exclude base price"
msgstr ""

#: wpc-composite-products.php:1682
msgid "\"Base price\" is the price set in the General tab. When \"Only base price\" is chosen, the total price won't change despite the price changes in variable components."
msgstr ""

#: wpc-composite-products.php:1686
msgid "Discount"
msgstr ""

#: wpc-composite-products.php:1693
msgid "The universal percentage discount will be applied equally on each component's price, not on the total."
msgstr ""

#: wpc-composite-products.php:1728
msgid "Same products"
msgstr ""

#: wpc-composite-products.php:1731
msgid "Allow"
msgstr ""

#: wpc-composite-products.php:1732
msgid "Do not allow"
msgstr ""

#: wpc-composite-products.php:1734
msgid "Allow/Do not allow the buyer to choose the same products in the components."
msgstr ""

#: wpc-composite-products.php:1738
msgid "Shipping fee"
msgstr ""

#: wpc-composite-products.php:1741
msgid "Apply to the whole composite"
msgstr ""

#: wpc-composite-products.php:1742
msgid "Apply to each component product"
msgstr ""

#: wpc-composite-products.php:1747
msgid "Custom display price"
msgstr ""

#: wpc-composite-products.php:1755
msgid "Above text"
msgstr ""

#: wpc-composite-products.php:1764
msgid "Under text"
msgstr ""

#: wpc-composite-products.php:1884
#: wpc-composite-products.php:1887
msgid "From"
msgstr ""

#: wpc-composite-products.php:2091
#: wpc-composite-products.php:2166
msgid "Qty:"
msgstr ""
