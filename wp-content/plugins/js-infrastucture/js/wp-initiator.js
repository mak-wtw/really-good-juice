jQuery.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	jQuery.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

class Wpinitiator {
	constructor() {
		//initialize here
		this.xhrObj = false;
		this.mutationObserver = false;
		this.notifySetting = {
			allow_dismiss: true,
			newest_on_top: false,
			mouse_over: null,
			delay: 3000,
			timer: 1000,
			offset:{
				x: 20,
				y:100
			},
			placement: {
				from: "top",
				align: "right"
			},
			animate: {
				enter: 'animated fadeInDown',
				exit: 'animated fadeOutUp'
			}
		}
		
	}

	create() {
		var self = this;
		jQuery(document).on('click', 'a:not(.skip-prevent)', function(event) {

			if (jQuery(this).hasClass('woocommerce-loop-product__link') || jQuery(this).hasClass('wc-block-grid__product-link')) {
				event.preventDefault();
				self.actionOnProductClick(this);
				return false; //skip product page load
			}

			var url = jQuery(this).attr('href');
			if (url.indexOf('/') >= 0) {
				event.preventDefault();
				self.loadAnchorData(this);
			}
		});

		jQuery(document).on('submit', 'form:not(.skip-prevent):not(.cart)', function(event) {
			var url = jQuery(this).attr('action');
			if (typeof url != 'undefined' && url != '' && url.indexOf('/') >= 0) {
				event.preventDefault();
				self.submitFormAndReloadSection(this, url);
			}
		});


		jQuery(document).on('click', 'button[name="add-to-cart"]', function(event) {
			event.preventDefault();
			event.stopPropagation();
			self.ajaxCartProduct(this);
		});

		jQuery(document.body).on('added_to_cart', function(event,fragments,cart_hash,target){
			if(typeof fragments!='undefined'){
				var targetEle = jQuery(target);
				
				jQuery.notifyDefaults(self.notifySetting);

				if(targetEle.closest('li').length>0){
					var title = targetEle.closest('li').find('.woocommerce-loop-product__title').text(),
					price = targetEle.closest('li').find('.woocommerce-Price-amount.amount').text();
					jQuery.notify({
						title: '<strong>'+title+'</strong>',
						message: price
					});
				} else if(targetEle.closest('div.summary-content.ps-container').length>0) {
					var pdpContainer = targetEle.closest('div.summary-content.ps-container'),
					title = pdpContainer.find('h1.product_title').text(),
					price = pdpContainer.find('.woocommerce-Price-amount.amount').text();
					if(pdpContainer.find('.wooco_total').length>0){
						price = pdpContainer.find('.wooco_total .woocommerce-Price-amount.amount').text();
					}
					jQuery.notify({
						title: '<strong>'+title+'</strong>',
						message: price
					});
				}

				if(typeof fragments['in_cart_qty']!='undefined'){
					var data, element, parent;
					for(var i in fragments['in_cart_qty']){
						data = fragments['in_cart_qty'][i];
						element = jQuery('a.add_to_cart_button[data-product_id="'+data.pid+'"]');
						if(element.length>0){
							parent = element.closest('li.product');
							parent.find('input.quantity[data-product-id="'+data.pid+'"]').val(data.qty);
							parent.find('div.shopAddToCart').removeClass('qtyHide').addClass('qtyShow');
						}
					}
				}

			}
		});

		window.onpopstate = function(e) {
			if (e.state) {
				document.getElementById("content").innerHTML = e.state.html;
				document.title = e.state.pageTitle;
			}
		}

		self.resetMutationObserver();
	}

	resetMutationObserver(){
		var toggleNode = document.querySelectorAll('a.add_to_cart_button');
		var self = this;
		if(self.mutationObserver){
			self.mutationObserver.disconnect();
		}
		self.mutationObserver = new MutationObserver(self.onupdateofAddtoCartClass);
		jQuery.each(toggleNode, function(){
			self.mutationObserver.observe(this, { attributes: true });
		});
	}

	onupdateofAddtoCartClass(mutationsList, observer) {
	    mutationsList.forEach(mutation => {
	        if (mutation.attributeName === 'class') {
	            let parentLi = jQuery(mutation.target).closest('li.product');
	            if(jQuery(mutation.target).hasClass('added')){
	            	parentLi.removeClass('loading');
	            	parentLi.addClass('added');
	            } else if(jQuery(mutation.target).hasClass('loading')){
	            	parentLi.removeClass('added');
	            	parentLi.addClass('loading');
	            }
	        }
	    });
	}

	actionOnProductClick(element) {
		var parentLi = jQuery(element).closest('li'),
			isComposite = parentLi.hasClass('product-type-composite');
		if (isComposite) {
			parentLi.find('a.quick_view').click();
		} else {
			parentLi.find('a.add_to_cart_button.ajax_add_to_cart').click();
		}
	}

	submitFormAndReloadSection(element, url) {
		var self = this,
			method = jQuery(element).attr('method');

		self.loadAndUpdateSections(url, jQuery(element).serialize(), method);
	}

	loadAnchorData(element) {
		var self = this,
			callUrl = jQuery(element).attr('href');
		self.loadAndUpdateSections(callUrl);
	}

	reloadSectionData() {
		jQuery(document).trigger('reloadSections');
	}

	updateQueryStringParameter(uri, key, value) {
		var re = new RegExp("([/?&])" + key + "[/=].*?([/&]|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			separator = uri[uri.indexOf(key) + key.length];
			return uri.replace(re, '$1' + key + separator + value + '$2');
		} else {
			return uri + separator + key + "=" + value;
		}
	}

	ajaxCartProduct(element) {
		var self = this,
			target = jQuery(element),
			form = target.closest('form'),
			callUrl = form.attr('action'),
			formData = form.serializeObject();

		callUrl = self.updateQueryStringParameter(callUrl,'wc-ajax','add_to_cart');
		
		delete formData['woosq-redirect'];

		formData["product_id"] =  target.attr('value');

		self._callAjax(callUrl, formData, {
			beforeSend: function() {
				target.removeClass('added');
				target.addClass('loading');
			},
			complete: function(response) {
				try {
					//this will remove msg
					if (typeof response.responseJSON != 'undefined') {
						var jcontent = response.responseJSON;
						target.removeClass('loading');
						if(typeof jcontent.fragments){
							const object1 = jcontent.fragments;
							for (const [key, value] of Object.entries(object1)) {
								jQuery(key).replaceWith(value);
							}
							target.addClass('added');
							/*form.append('<div id="temp_msg" class="woocommerce"><div class="woocommerce-message" role="alert">This product has been added to your cart.</div></div>');
							setTimeout(function(){
								jQuery('#temp_msg').remove();
							},4000);*/
							jQuery( document.body ).trigger( 'added_to_cart', [ jcontent.fragments, jcontent.cart_hash, target ] );
							jQuery.magnificPopup.close();
						}
					}
				} catch (e) {
					target.removeClass('loading');
				}
			},
			error: function(response) {
				target.removeClass('loading');
			}
		}, "POST");
	}

	loadAndUpdateSections(callUrl, formdata, method) {
		var self = this;

		//check if menu is open:
		var hasMenuOpen = jQuery('.main-navigation').hasClass('toggled');
		if(hasMenuOpen){
			jQuery('.main-navigation.toggled').find('button.menu-toggle').trigger('click');
		}

		self._callAjax(callUrl, formdata, {
			beforeSend: function() {
				self.loadMessage('Loading...', 'notice');
			},
			complete: function(response) {
				try {
					//this will remove msg
					if (typeof response.responseJSON != 'undefined') {
						var jcontent = response.responseJSON;
					} else if (typeof response.responseText != 'undefined') {
						let doc = new DOMParser().parseFromString(response.responseText, 'text/html'),
							htmlContent,
							breadCrumb,
							noscripts,
							styles,
							scripts,
							linkStyle,
							breadCrumbEle = jQuery('.woocommerce-breadcrumb'),
							toPasteElem = jQuery('#content').length > 0 ? jQuery('#content') : jQuery('#site-content');

						toPasteElem.html('');
						self.loadMessage('Loading...', 'notice');
						jQuery("html, body").animate({ scrollTop: 0 }, "slow");
						//site-content
						htmlContent = doc.querySelector('#' + toPasteElem.attr('id'));
						toPasteElem.html(htmlContent);

						//site-content
						noscripts = doc.getElementsByTagName('noscript');
						styles = doc.getElementsByTagName('style');
						scripts = doc.getElementsByTagName('script');
						//linkStyle = doc.querySelectorAll("link[rel='stylesheet']");

						//jQuery("style").remove();
						jQuery('head').append(styles);

						//jQuery("noscript").remove();
						jQuery('head').append(noscripts);

						//jQuery("link[rel='stylesheet']").remove();
						//jQuery('head').append(linkStyle);	

						setTimeout(function() {
							jQuery('script').remove();
							jQuery('body').append(scripts);
							jQuery(window).trigger('load');
							self.resetMutationObserver();
						}, 1500);

						

						breadCrumb = doc.querySelector('.woocommerce-breadcrumb');
						breadCrumbEle.html(breadCrumb);

						jQuery(document).trigger('ready');

						self.pushBrowserState(htmlContent.innerHTML, doc.title, callUrl);
						/*setTimeout(function() {
							document.dispatchEvent(new Event("DOMContentLoaded"));
						}, 500);*/

					}
				} catch (e) {
					self.loadMessage('', '');
				}
			}
		}, method);
	}
	_callAjax(url, formdata, additional, method) {
		var self = this,
			config,
			requestMethod = method || 'GET',
			onComplete, newconfig;

		onComplete = function(response) {};

		/*if (method && method == 'POST') {
			if (additional && typeof additional.complete == "function") {
				onComplete = function(response) {
					(additional.complete)(response);
					self.reloadSectionData();
				}
				delete additional.complete;
			}
		} else {
			if (additional && typeof additional.complete == "function") {
				onComplete = additional.complete;
				delete additional.complete;
			}
		}*/



		config = {
			type: 'POST',
			data: formdata,
			showLoader: true,
			dataType: 'json',
			beforeSend: function() {

			},
			complete: onComplete,
			error: function(response) {
				self.loadMessage('Error! Something went wrong.', 'error', true);
			}
		};

		newconfig = jQuery.extend({}, config, additional);
		if (self.xhrObj) {
			self.xhrObj.abort();
		}
		self.xhrObj = jQuery.ajax(url, newconfig);
	}

	pushBrowserState(responseContent, pageTitle, urlPath) {
		document.getElementById("content").innerHTML = responseContent;
		document.title = pageTitle;
		window.history.pushState({
			"html": responseContent,
			"pageTitle": pageTitle
		}, "", urlPath);
	}

	loadMessage(msg, type, autoDestroy) {
		var self = this,
			messageTemplate = "<div class='call-messgae " + type + "'>" + msg + "</div>";
		jQuery('.call-messgae').remove();
		if (msg != '')
			jQuery('#content').prepend(messageTemplate);

		if (typeof autoDestroy != '' && autoDestroy == true) {
			setTimeout(function() {
				jQuery('.call-messgae').remove();
			}, 4000);
		}
	}
}

var wpinitiator = new Wpinitiator();

wpinitiator.create();