<?php
   /*
   Plugin Name: Woo QTY
   Version: 1.1
   Description: Adds professionally looking "-" and "+" buttons around product quantity field, on product and cart page.
   Author: Archita

   */

register_activation_hook( __FILE__, 'wooqty_activate' );
function wooqty_activate() {  
    // Prevent plugin activation if the minimum PHP version requirement is not met.
    if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
        deactivate_plugins( basename( __FILE__ ) );
        $msg = '<p><strong>Qty Increment Buttons for WooCommerce</strong> requires PHP version 5.4 or greater. Your server runs ' . PHP_VERSION . '.</p>';
        wp_die( $msg, 'Plugin Activation Error',  array( 'response' => 200, 'back_link' => TRUE ) );
    }
    // Store time of first plugin activation (add_option does nothing if the option already exists).
    add_option( 'wooqty_first_activate', time());
}

add_action('wp_enqueue_scripts', 'wp_enqueue_child_files'); // Register Our Child Theme JS & CSS File

function wp_enqueue_child_files()
{
   wp_enqueue_script('wooqtyJS', plugin_dir_url( __FILE__ ) . '/assets/js/main.js', array('jquery'), false, true);
   wp_enqueue_style('wooqtyCss', plugin_dir_url( __FILE__ ) . '/assets/css/style.css', array(), false, 'all');
   wp_localize_script('wooqtyJS', 'ajax_qty', array('ajaxurl' =>admin_url('admin-ajax.php')));
}

// Remove Add To cart Button
//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

// Add our Quanity Input
add_action('woocommerce_after_shop_loop_item', 'QTY');
function QTY()
{
    global $product;
    $product = wc_get_product(get_the_ID());
    foreach ( WC()->cart->get_cart() as $cart_item ) { 
    if($cart_item['product_id'] == $product->get_id() ){
       $qty =  $cart_item['quantity'];
     }
}
$proType = $product->get_type();


/*if($qty >= 1){*/
    ?>
        <div class="shopAddToCart <?php if(Check_if_product_in_cart($product->get_id())['QTY'] > 0){ echo 'qtyShow';}else{ echo 'qtyHide';}?>">
        <?php if($proType == 'simple'){
            echo '  <button  value="-" class="minus"  >-</button>';
        } ?>
      
        <input type="text"
        disabled="disabled"
        size="2"
        value="<?php echo (Check_if_product_in_cart($product->get_id())) ? Check_if_product_in_cart($product->get_id())['QTY'] : 0;
    ?>"
        id="count"
        data-product-id= "<?php echo $product->get_id() ?>"
        data-in-cart="<?php echo (Check_if_product_in_cart($product->get_id())) ? Check_if_product_in_cart($product->get_id())['in_cart'] : 0;
    ?>"
        data-in-cart-qty="<?php echo (Check_if_product_in_cart($product->get_id())) ? Check_if_product_in_cart($product->get_id())['QTY'] : 0;
    ?>"
        class="quantity  qty"
        max_value = "100"
        min_value = <?php echo $product->get_min_purchase_quantity(); ?>
        >
        <?php if($proType == 'simple'){
            echo '<button type="button" value="+" class="plus"  >+</button>';
        } ?>
       

        </div>
                              <?php
} /*}*/
//Check if Product in Cart Already
function Check_if_product_in_cart($product_ids)
{

    foreach (WC()->cart->get_cart() as $cart_item):

        $items_id = $cart_item['product_id'];
        $QTY = $cart_item['quantity'];

        // for a unique product ID (integer or string value)
        if ($product_ids == $items_id):
            return ['in_cart' => true, 'QTY' => $QTY];

        endif;

    endforeach;
}
//Add Event Handler To update QTY
add_action('wc_ajax_update_qty', 'update_qty');
//add_action( 'wp_ajax_nopriv_update_qty', 'update_qty' );.
function update_qty()
{
    ob_start();
    $product_id = absint($_POST['product_id']);
    $product = wc_get_product($product_id);
    $quantity = $_POST['quantity'];

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item):

        if ($cart_item['product_id'] == $product_id) {
            WC()->cart->set_quantity($cart_item_key, $quantity, true);
        }

    endforeach;

    wp_send_json('done');
}

