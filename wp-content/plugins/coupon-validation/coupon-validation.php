<?php
   /*
   Plugin Name: Coupon Validation
   Version: 1.1
   Description: Checking coupon code W.R.T shipping options
   Author: Archita

   */

register_activation_hook( __FILE__, 'coupon_custom_activate' );
function coupon_custom_activate() {  
    // Prevent plugin activation if the minimum PHP version requirement is not met.
    if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
        deactivate_plugins( basename( __FILE__ ) );
        $msg = '<p><strong> Checking coupon code for WooCommerce</strong> requires PHP version 5.4 or greater. Your server runs ' . PHP_VERSION . '.</p>';
        wp_die( $msg, 'Plugin Activation Error',  array( 'response' => 200, 'back_link' => TRUE ) );
    }
    // Store time of first plugin activation (add_option does nothing if the option already exists).
    add_option( 'coupon_custom_first_activate', time());
}


// Add a custom checkbox to Admin coupon settings pages
add_action( 'woocommerce_coupon_options', 'add_coupon_option_checkbox', 10 );
function add_coupon_option_checkbox() {
        woocommerce_wp_select(
          array(
            'id'      => 'shipping_coupon',
            'label'   => __( 'Shipping option', 'woocommerce' ),
            'options'     => array(
                        ''        => __( 'Select Shipping option', 'woocommerce' ),
                        'eatin_order'    => __('EatIn', 'woocommerce' ),
                        'collection_order' => __('Collection', 'woocommerce' ),
                        'delivery_order' => __('Delivery', 'woocommerce' ),
                    )
          )
        );
}

// Save the custom checkbox value from Admin coupon settings pages
add_action( 'woocommerce_coupon_options_save', 'save_coupon_option_checkbox', 10, 2 );
function save_coupon_option_checkbox( $post_id, $coupon ) {
    update_post_meta( $post_id, 'shipping_coupon', isset( $_POST['shipping_coupon'] ) ? $_POST['shipping_coupon'] : 'no' );
}

/*************Woocommerce coupon code validation on checkout click******************/
/*add_action( 'woocommerce_checkout_process', 'wpse_woocommerce_checkout_process' );
function wpse_woocommerce_checkout_process() {
  if( WC()->cart->get_coupons() ) {
  foreach ( WC()->cart->get_coupons() as $code => $coupon ) :
    $cou_name:  esc_attr( sanitize_title( $code ) );
  endforeach;
  }
    
    return $cou_name;
}*/
/*Array
(
    [coupon_code] => eat50
    [apply_coupon] => Apply coupon
)*/


add_action('woocommerce_applied_coupon', 'apply_product_on_coupon');
function apply_product_on_coupon( ) {
    global $woocommerce;
    /*echo "<pre>";print_r($_POST);*/
    $applied_coupon_name = $_POST['coupon_code'];
    global $woocommerce;
    $c = new WC_Coupon($applied_coupon_name);
    $applied_coupon_id = $c->id; 
    ?>
    <script>
     
      
    </script>
   <?php
    die();
    /*$coupon_id = '12345';
    $free_product_id = 54321;

    if(in_array($coupon_id, $woocommerce->cart->get_applied_coupons())){
        $woocommerce->cart->add_to_cart($free_product_id, 1);
    }*/
}
?>